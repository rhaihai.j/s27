const http = require('http');

let directory = [
	{
		"name":"Brandon",
		"email":"brandon@gmail.com"
	},
	{
		"name":"Jobert",
		"email":"jobert@gmail.com"
	}
	];

const server = http.createServer((req,res)=>{
if((req.url == '/users' || req.url == '/users.html') && req.method == "GET"){
	res.writeHead(200,{'Content-Type':'text/html'});
	res.write(JSON.stringify(directory));
	res.end();
	//res.end('<p>Data retrieved from database</p>');
}

if((req.url == '/users' || req.url == '/users.html') && req.method == "POST"){
	// res.writeHead(200,{'Content-Type':'text/plain'});
	// res.end('Data to be sent to the database');
	let requestBody = '';

	req.on('data',(data)=>{
		requestBody += data;
	});

	req.on('end',()=>{
		//console.log(typeof requestBody);
		requestBody = JSON.parse(requestBody);

		let newUser = {
		"name":requestBody.name,
		"email":requestBody.email
	}

	directory.push(newUser);
	console.log(directory);

	res.writeHead(200,{'Content-type':'application/json'});
	res.write(JSON.stringify(newUser));
	res.end();
	});

	

	

	}


});
server.listen(3000,'localhost',()=>{
	console.log('Listening to port 4000');
});